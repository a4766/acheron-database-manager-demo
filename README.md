##### About this repo
# Demo setup Acheron Database Manager
![Acheron Database Manager](https://acheron.cloud/logo-acheron/ "Acheron")

Welcome to this repositry ✨

<table>
<tbody>
<tr>
  <td>
    <h2>📖 Documentation Acheron</h2>
    <p>
      Read <a href="https://docs.acheron.cloud">Online Docs</a>.
    </p>
  </td>
  <td>
    <h2>🐞 Reporting bugs</h2>
    <p>
      Check out <a href="https://gitlab.com/a4766/demo-adm/-/issues/new">Reporting Bugs</a> page.</p>
  </td>
</tr>
<tr>
  <td>
    <h2>📖 Documentation Gitlab </h2>
    <p>
       Read  <a href="https://docs.gitlab.com/ee/ci/pipeline">Online Docs</a>
    </p>
  </td>
  <td>
    <h2>❔ Questions</h2>
    <p>
      Ask out <a href="ask@acheron.cloud">Getting Help</a>
    </p>
  </td>
</tr>
</tbody>
</table>

## Try Out
- create config files and check the output older for generated SQL statements
- create a template file with config for reuse abilities
- check log files for auditability and control

## Repository setup
The demo setup contains the following setup
<ol>
 <li>environment folder - containg license file and environment info
 <li>input folder - contains configurations files
 <li>templates folder - contains demo subset templates (more are available)
 <li>.gitignore - contains info on blocking files/folders
 <li>.gitlab-ci.yml - contains pipeline details
 <li>README.md - contains details demo repo
</ol>

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files
- [ ] [Login](https://gitlab.com/users/sign_in) to your account
- [ ] [Request access](https://gitlab.com/a4766/acheron-database-manager-demo/-/project_members/request_access) on the repository
- [ ] [Create](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) a branch
- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:faf65b5e1310d171f7bf0675aeb82f0d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:faf65b5e1310d171f7bf0675aeb82f0d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:faf65b5e1310d171f7bf0675aeb82f0d?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/a4766/acheron-database-manager-demo.git
git branch -M main
git push -uf origin main
```

### Question can be send to <ask@acheron.cloud>

